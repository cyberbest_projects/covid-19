<?php
    include_once("config/access.php");
    if(isset($_POST["btn-location"])){
         $location = clean($conn, $_POST["btn-location"]);
         
        #check variables
        if($location!="" && $location<4){
            //Submit all data to DB and move to last page
            $rep_location = "INSERT INTO $rep_location_tb (rep_location, rep_location_userID) VALUE('$location', '$sess_userID')";
            $qry_rep_location = $conn->query($rep_location);

            if($qry_rep_location){
                
                header("Location: self_report3.php");
            }
        }else{
            $rep_location = "INSERT INTO $rep_location_tb (rep_location, rep_location_userID) VALUE('$location', '$sess_userID')";
            $qry_rep_location = $conn->query($rep_location);
            
            header("Location: self_report4.php");
        }

    }
?>

<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Team C-19 - Lets heal the world</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico in the root directory -->
    <link rel="shortcut icon" type="image/png" href="assets/images/icon/c-19-icon.png">
    <!-- all css here -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/slicknav.min.css">
    <link rel="stylesheet" href="assets/css/swiper.min.css">
    <link rel="stylesheet" href="assets/css/typography.css">
    <link rel="stylesheet" href="assets/css/default-css.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <!-- modernizr css -->
    <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- header area start -->
    <header>        
        <div class="header-area h-style5">
            <div clas="row" style="background-color: #0a034b !important;">
                <div class="container text-white">
                    <div class="row">
                        <div class="col-md-8"></div>
                        <div class="col-md-4 text-right">
                            <a href="javascript:void(0)"><i class="fa fa-user"></i></a>
                            <span>Language:</span>
                            <img src="assets/images/web/uk.png" width="5%">
                           <i class="fa fa-search"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <div class="logo">
                            <a href="index.html">
                                <img src="assets/images/web/c-19-logo.png" alt="logo">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-9 d-none d-lg-block">
                        <div class="main-menu">
                            <nav id="nav_mobile_menu">
                                <ul id="navigation">
                                    <li class="active">
                                        <a href="index.php#home">Home</a>
                                    </li>
                                    <li>
                                        <a href="index.php#how_it_works">How it works</a>
                                    </li>
                                    <li>
                                        <a href="index.php#self_report">Self Report</a>
                                    </li>
                                    <!--
                                    <li>
                                        <a href="#price">Pricing Plan</a>
                                    </li>
                                    -->
                                    <li>
                                        <a href="index.php#join_us">Join us</a>
                                    </li>
                                    <li>
                                        <a href="index.php#contact">Contact Us</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="col-12 d-block d-lg-none">
                        <div id="mobile_menu"></div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header area end -->

    <!-- service area start -->
    <div class="service-area pt--70 pb--100">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 ">
                    <h3>COVID-19 Self Reporting</h3>
                    
                    <form id="self-form" class="mt-3" method="POST" action="">
                        <h4>Where are you?</h4>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary form-control stylet" name="btn-location" value="1">
                                I am at home, Haven’t visited the Hospital for Suspected C-19
                            </button>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary form-control stylet" name="btn-location" value="2">
                                I am at the Hospital with suspected C-19 symptoms
                            </button>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary form-control stylet" name="btn-location" value="3">
                                I am back from the Hospital, do like to talk about my treatment
                            </button>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary form-control stylet" name="btn-location" value="4">
                                Am back from the Hospital, I have told you about my treatment
                            </button>
                        </div>
                    </form>

                </div>
                <div class="col-md-6">
                    <img src="assets/images/web/c-19-web-phone1.png" class="img-fluid" width="80%">
                </div>
            </div>
        </div>
    </div>
    <!-- service area end -->
      

    <!-- app-download area start -->
    <section class="download-area" id="join_us">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="download-title">
                        <h2>Join us</h2>
                        <p>We can't do it alone we need your support, collaboration and partnership</p>
                    </div>
                </div>
                <div class="col-md-10 offset-md-1">
                    <div class="download-app">
                        <a class="single-download-links" href="#">
                            <div class="sdl-icon">
                                <i class="fa fa-code"></i>
                            </div>
                            <div class="sdl-content">
                                <p>Became a Team C-19</p>
                                <h2>Member</h2>
                            </div>
                        </a>
                        <a class="single-download-links" href="#">
                            <div class="sdl-icon">
                                <i class="fa fa-code-fork"></i>
                            </div>
                            <div class="sdl-content">
                                <p>Support in fighting C-19 </p>
                                <h2>Support</h2>
                            </div>
                        </a>
                        <a class="single-download-links" href="#">
                            <div class="sdl-icon">
                                <i class="fa fa-group"></i>
                            </div>
                            <div class="sdl-content">
                                <p>Partner with us now</p>
                                <h2>Partener</h2>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- app-download area end -->

    <!-- footer area start -->
    <div class="footer-area ptb--50">
        <div class="container">
            <div class="footer-content">
                <div class="row">
                    <div class="col-md-6 text-left">&copy; 2020 <strong>Team C-19</strong>. All Rights Reserved.</div>

                    <div class="col-md-6 text-right">
                        <a href="javascript:void(0)">FAQ</a> / <a href="javascript:void(0)">Privacy Policy </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer area end -->

    <!-- jquery latest version -->
    <script src="assets/js/vendor/jquery-2.2.4.min.js"></script>
    <!-- bootstrap 4 js -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- others plugins -->
    <script src="assets/js/jquery.slicknav.min.js"></script>
    <script src="assets/js/counterup.min.js"></script>
    <script src="assets/js/waypoints.js"></script>
    <script src="assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="assets/js/isotope.pkgd.min.js"></script>
    <script src="assets/js/jquery.magnific-popup.min.js"></script>
    <script src="assets/js/countdown.js"></script>
    <script src="assets/js/swiper.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/jquery.zoomslider.min.js"></script>
    <script src="assets/js/jquery.firefly.js"></script>
    <!-- google map -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBO_5h890WNShs_YLGivCBfs2U89qXR-7Y&callback=initMap"></script>
    <script src="assets/js/scripts.js"></script>
</body>

</html>