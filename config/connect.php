<?php
/* Database connection */

$servername = "localhost";
$username = "root";
$password = "";
$database = "covid19";

//create connection
$conn = new mysqli($servername, $username, $password, $database);

//check connection
if($conn->connect_error){
    die("Connection failed: " . $conn->connect_error);
}

//DATABASE TABLES
$users_tb = "users";
$report_tb = "reporting";
$rep_location_tb = "reporters_location";
$rep_treatment_tb = "rep_treatment";


$all_users = "SELECT * FROM $users_tb";
$qry_users = $conn->query($all_users);
$rows_users = $qry_users->fetch_assoc();

function clean($conn, $data){
    $data=trim($data);
    $data=stripslashes($data);
    $data=htmlspecialchars($data);
    $data=mysqli_real_escape_string($conn,$data);
    return $data;
}


?>