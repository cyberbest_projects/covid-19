<div class="footer-area ptb--50">
    <div class="container">
        <div class="footer-content">
            <div class="row">
                <div class="col-md-6 text-left">&copy; 
                    <script> 
                        var d= new Date().getFullYear();
                        document.write(d)
                    </script>
                 <strong>Team C-19</strong>. All Rights Reserved.</div>

                <div class="col-md-6 text-right">
                    <a href="faq.php">FAQ</a> / <a href="privacy-policy.php">Privacy Policy </a>
                </div>
            </div>
        </div>
    </div>
</div>