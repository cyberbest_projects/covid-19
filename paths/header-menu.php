<header>        
        <div class="header-area h-style5">
            <div clas="row" style="background-color: #0a034b !important;">
                <div class="container text-white">
                    <div class="row">
                        <div class="col-md-8"></div>
                        <div class="col-md-4 text-right"><span>Language:</span>

                        

                            <img src="assets/images/web/uk.png" width="5%">
                           <!-- <i class="fa fa-search"></i> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <div class="logo">
                            <a href="index.html">
                                <img src="assets/images/web/c-19-logo.png" alt="logo">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-9 d-none d-lg-block">
                        <div class="main-menu">
                            <nav id="nav_mobile_menu">
                                <ul id="navigation">
                                    <li class="active">
                                        <a href="#home">Home</a>
                                    </li>
                                    <li>
                                        <a href="#how_it_works">How it works</a>
                                    </li>
                                    <li>
                                        <a href="#self_report">Self Report</a>
                                    </li>
                                    <!--
                                    <li>
                                        <a href="#price">Pricing Plan</a>
                                    </li>
                                    -->
                                    <li>
                                        <a href="#join_us">Join us</a>
                                    </li>
                                    <li>
                                        <a href="#contact">Contact Us</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="col-12 d-block d-lg-none">
                        <div id="mobile_menu"></div>
                    </div>
                </div>
            </div>
        </div>
    </header>