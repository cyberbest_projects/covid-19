<?php
    if(isset($_GET['msg'])){
        echo "<div class='text-center alert alert-success'>".$_GET['msg']."</div>";
    }
    if(isset($_GET['error'])){
        echo "<div class='text-center alert alert-warning'>".$_GET['error']."</div>";
    }
?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Team C-19 - Lets heal the world</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="COVID-19 Symptom Tracker used for reduction of the spread of corona virus">
    <!-- Place favicon.ico in the root directory -->
    <link rel="shortcut icon" type="image/png" href="assets/images/icon/c-19-icon.png">
    <!-- all css here -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/slicknav.min.css">
    <link rel="stylesheet" href="assets/css/swiper.min.css">
    <link rel="stylesheet" href="assets/css/typography.css">
    <link rel="stylesheet" href="assets/css/default-css.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <!-- modernizr css -->
    <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>
    <!-- Phone input -->
    <link rel="stylesheet" href="assets/build/css/intlTelInput.css">

</head>

<body>
    
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- header area start -->
    <?php include_once("paths/header-menu.php"); ?>
    <!-- header area end -->
    <!-- hero area start -->
    <section class="hero-area app-slider pos-rel full-height d-flex align-items-center parallax" id="home" data-speed="3" data-bg-image="assets/images/web/c-19-web-bg1.png">
        <!-- <div class="slider_overlay"></div> -->
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <div class="hero-content">
                        <h2>COVID-19 TRACKER</h2>
                        <h3>Daily self-report</h3>
                        <h3>Help slow COVID-19 spread</h3>
                        <h3>Identify those at risk soon</h3>
                        <div class="links pull-left">
                            <!-- <a class="" href="#">GET APP <i class="fa fa-android"></i></a> -->
                            <img src="assets/images/web/comingsoon-andriod.png" width="80%">
                        </div>
                        <div class="links pull-left">
                            <img src="assets/images/web/comingsoon-ios.png" width="80%">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="screen-slides-area">
                        <img src="assets/images/web/mobile-mockup.png" alt="image">
                        <div class="screen-slides swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <img src="assets/images/web/c-19-web-averter.png" alt="image" width="100%">
                                </div>
                                <div class="swiper-slide">
                                    <img src="assets/images/web/c-19-web-phone2.png" alt="image" width="90%">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </section>
    <!-- hero area end -->
    <!-- service area start -->
    <div class="service-area pt--70 pb--100">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="assets/images/web/c-19-web-phone1.png" class="img-fluid" width="80%">
                </div>
                <div class="col-md-6">
                    <div class="appcta-content">
                        <h2>
                            All you need is 60 sec to carry out self-report daily, even if you are well.
                        </h2>
                        <h3>Help Identify?</h3>
                        <div class="listed-list">
                            <ul>
                                <li><i class="fa fa-check-circle"></i> High risk areas </li>
                                <li><i class="fa fa-check-circle"></i> Who is most at risk</li>
                                <li><i class="fa fa-check-circle"></i> Slow C-19 spread</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- service area end -->
    
    <!-- app-cta area start -->
    <div class="app-cta-area pb--100 bg-gray" id="app-feature">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-5">
                    <div class="mscreen-left effectupdown2">
                        <img src="assets/images/team/team-best-onyekachi.jpg" alt="image">
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="appcta-content appcta-right">
                        <h2 style="line-height: 100% !important;">Thank you for your support by joining Team C-19 and millions of people in the fight agains COVID-19.</h2>
                        <p>
                            Every information given is essential as this data will be used in giving a better understanding to the study and research of COVID-19 pandemic which is ravaging our society. 
                            Every trust granted us is taking seriously, and we ensuring that the information given are well secured. We are also working hard to improve and lunch the app based on users feedback. 
                        </p>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- app-cta area end -->
    <!-- appfeature area strat -->
    <section class="app-feature-area ptb--100" id="how_it_works" style="background-color: #C5EBFF;">
        <div class="container">
            <div class="h5-title section-title">
                <h2>How it works</h2>
            </div>
            <div class="row">
                <div class="d-none d-lg-block col-lg-4 order-lg-1">
                    <div class="ft-device text-center">
                        <img src="assets/images/web/c-19-web-phone2.png" width="80%" alt="image">
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 order-lg-0">
                    <div class="feature-item">
                        <div class="icon">
                            <i class="fa fa-internet-explorer"></i>
                        </div>
                        <div class="content">
                            <h4>Web App</h4>
                            <p>Visit Team C-19 Web App page, Login or register if you don't have an account
                            </p>
                        </div>
                    </div>
                    <div class="feature-item">
                        <div class="icon">
                            <i class="fa fa-mobile-phone"></i>
                        </div>
                        <div class="content">
                            <h4>Mobile App</h4>
                            <p>Dowload and install the Android, IOS and Windows version of the app on your mobile phone device.
                            </p>
                        </div>
                    </div>
                    <div class="feature-item">
                        <div class="icon">
                            <i class="fa fa-asl-interpreting"></i>
                        </div>
                        <div class="content">
                            <h4>Connection</h4>
                            <p>You must login to establish a connection with Team C-19 database, if you dont have an account you can register.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 order-lg-2 right-side">
                    <div class="feature-item">
                        <div class="icon">
                            <i class="fa fa-life-bouy"></i>
                        </div>
                        <div class="content">
                            <h4>Health Status</h4>
                            <p>Tell us your health status, by picking and option from the list.
                            </p>
                        </div>
                    </div>
                    <div class="feature-item">
                        <div class="icon">
                            <i class="fa fa-bullhorn"></i>
                        </div>
                        <div class="content">
                            <h4>Talk to us</h4>
                            <p>it takes less than 60 seconds for you to Answer few questions, as required by C-19 App.
                            </p>
                        </div>
                    </div>
                    <div class="feature-item">
                        <div class="icon">
                            <i class="fa fa-share-alt"></i>
                        </div>
                        <div class="content">
                            <h4>Training</h4>
                            <p>Visit, repeat and the next day, share app with love once friends and family to help us track C-19.

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- appfeature area end -->
    
    <!-- trainer area start -->
    <section class="trainer-area ptb--100" id="self_report">
        <div class="container">
            <div class="h5-title section-title">
                <h2>Self Report</h2>
            </div>
            <div class="row">
                <!--
                <div class="col-lg-4 col-md-6">
                    <div class="single-trainer trainer_s_three">
                        <div class="thumb">
                            <img src="assets/images/team/team-ayo-clinton.jpg" alt="Ayo Clinton's image">
                        </div>
                        <div class="content">
                            <h4>Ayo Clinton</h4>
                            <p>CTO</p>
                            <ul class="social">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
                -->
                <div class="col-lg-6 col-md-6">
                    
                </div>
                <div class="col-lg-6 col-md-6">
                    
                </div>
                <div class="col-md-6">
                    
                    <div class="contact-content cnt-form">
                        
                        <form action="actions.php" method="POST" autocomplete="off">
                            <h2>Sign up</h2>
                            <div class="row pt--10">
                                <div class="col-md-12 mb-4">
                                    <input id="phone" type="tel" class="iti" name="user_phone" required="required">
                                </div>
                                <div class="col-md-12">
                                    <input type="text" placeholder="Enter Your Fullname" name="user_name" required="required">
                                </div>
                                <div class="col-md-12">
                                    <input type="text" placeholder="Enter Your Contact Address" name="user_contact" required="required">
                                </div>
                                <div class="col-md-12">
                                    <input type="password" placeholder="Enter Your Password" name="user_password" required="required">
                                </div>
                                <div class="col-12 text-center">
                                    <input type="submit" name="btn-send" value="Sign up">
                                    <p class="pt--10">Already have an account? Sign in</p>
                                </div>
                            </div>
                            
                        </form>
                       
                    </div>
                </div>
                                            
                <div class="contact-content">
                    
                    <form action="actions.php" method="POST">
                        <h2>Sign in</h2>
                        <div class="row pt--10">
                            <div class="col-md-12">
                                <input type="text" placeholder="Enter Your Phone Number" name="user_phone">
                            </div>
                            <div class="col-md-12">
                                <input type="password" placeholder="Enter Your Password" name="user_password">
                            </div>
                            <div class="col-12 text-center">
                                <input type="submit" name="btn-sign-in" value="Sign in">
                                <p class="pt--10"><a href="recover_pass.php">Recover your password</a></p>
                            </div>
                            
                            </div>
                        </div>
                        
                    </form>
                    
                </div>
                
            </div>
        </div>
    </section>
    <!-- trainer area end -->

    <!-- pricing area start 
    <section class="pricing-area ptb--100" id="price">
        <div class="container">
            <div class="h5-title section-title">
                <h2>Clients</h2>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <img src="assets/images/clients/kbest2.jpg">
                </div>
                <div class="col-lg-3 col-md-6">
                        <img src="assets/images/clients/Koxbit.png">
                    </div>
                <div class="col-lg-3 col-md-6">
                    <img src="assets/images/clients/S1-logo.png">
                </div>
                <div class="col-lg-3 col-md-6">
                    <img src="assets/images/clients/ph.png">
                </div>
            </div>
        </div>
    </section>
     pricing area end -->

    <!-- app-download area start -->
    <section class="download-area" id="join_us">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="download-title">
                        <h2>Join us</h2>
                        <p>We can't do it alone we need your support, collaboration and partnership</p>
                    </div>
                </div>
                <div class="col-md-10 offset-md-1">
                    <div class="download-app">
                        <a class="single-download-links" href="#">
                            <div class="sdl-icon">
                                <i class="fa fa-code"></i>
                            </div>
                            <div class="sdl-content">
                                <p>Became a Team C-19</p>
                                <h2>Member</h2>
                            </div>
                        </a>
                        <a class="single-download-links" href="#">
                            <div class="sdl-icon">
                                <i class="fa fa-code-fork"></i>
                            </div>
                            <div class="sdl-content">
                                <p>Support in fighting C-19 </p>
                                <h2>Support</h2>
                            </div>
                        </a>
                        <a class="single-download-links" href="#">
                            <div class="sdl-icon">
                                <i class="fa fa-group"></i>
                            </div>
                            <div class="sdl-content">
                                <p>Partner with us now</p>
                                <h2>Partener</h2>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="align-content-center" style="background-color:#00AFEF;" id="donate">
        <!--
        <div class="container pt--50 pb--50">
            <div class="row ">
                <div class="col-md-6 text-right">
                    <h2 class="title">Give a $1 and Save <br>a life today!</h2>
                </div>
                <div class="col-md-6">
                    <button name="donate" class="donate">Donate</button>
                </div>
            </div>
        </div> -->
    </section>
    <!-- app-download area end -->
    
    <!-- contact area start -->
    <section class="contact-area h5-contact ptb--100" id="contact">
        <div class="container">
            <div class="h5-title section-title">
                <span>Locate Us</span>
                <h2>Contact</h2>
            </div>
            <div class="row align">
                <div class="col-md-6">
                    <div class="contact-content cnt-form">
                        <form action="#">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" placeholder="Enter Your Name" name="name">
                                </div>
                                <div class="col-md-6">
                                    <input type="email" placeholder="Enter Your Email" name="email">
                                </div>
                                <div class="col-md-12">
                                    <textarea name="msg" id="msg" placeholder="Enter Your Message"></textarea>
                                </div>
                                <div class="col-12">
                                    <input type="submit" name="send" value="send">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-6">
                    <h2>Emegency Numbers</h2>
                    <h4>
                        NCDC: <i class="text-primary fa fa-mobile-phone"> </i> +234 800 9700 0010 
                        <i class="text-primary fa fa-mobile-phone"></i> +234 802 316 9485
                    </h4>
                </div>
            </div>
        </div>
    </section>
    <!-- contact area end -->
    <!-- footer area start -->
    <?php include_once("paths/footer-area.php"); ?>
    <!-- footer area end -->

    <!-- jquery latest version -->
    <script src="assets/js/vendor/jquery-2.2.4.min.js"></script>
    <!-- bootstrap 4 js -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- others plugins -->
    <script src="assets/js/jquery.slicknav.min.js"></script>
    <script src="assets/js/counterup.min.js"></script>
    <script src="assets/js/waypoints.js"></script>
    <script src="assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="assets/js/isotope.pkgd.min.js"></script>
    <script src="assets/js/jquery.magnific-popup.min.js"></script>
    <script src="assets/js/countdown.js"></script>
    <script src="assets/js/swiper.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/jquery.zoomslider.min.js"></script>
    <script src="assets/js/jquery.firefly.js"></script>
    <!-- google map -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBO_5h890WNShs_YLGivCBfs2U89qXR-7Y&callback=initMap"></script>
    <script src="assets/js/scripts.js"></script>
    <!-- Use as a Vanilla JS Phone plugin -->
    <script src="assets/build/js/intlTelInput.js"></script>

    <script>
    var input = document.querySelector("#phone");
    window.intlTelInput(input, {
      // allowDropdown: false,
      // autoHideDialCode: false,
      // autoPlaceholder: "off",
      // dropdownContainer: document.body,
      // excludeCountries: ["us"],
      // formatOnDisplay: false,
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
      // hiddenInput: "full_number",
      // initialCountry: "auto",
      // localizedCountries: { 'de': 'Deutschland' },
      // nationalMode: false,
      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
      // placeholderNumberType: "MOBILE",
      // preferredCountries: ['cn', 'jp'],
      // separateDialCode: true,
      utilsScript: "assets/build/js/utils.js",
    });
  </script>

</body>

</html>